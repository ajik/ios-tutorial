//
//  ViewController.swift
//  app-shoes
//
//  Created by Indoalliz on 02/03/19.
//  Copyright © 2019 Indoalliz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var UIAdidasLogoView: UIImageView!
    @IBOutlet weak var UIBGImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        UIAdidasLogoView.frame = CGRect(x: view.frame.size.width / 2 - UIAdidasLogoView.frame.size.width / 2, y: 50, width: UIAdidasLogoView.frame.size.width, height: UIAdidasLogoView.frame.size.height)
        
        UIBGImage.frame = view.frame
    }


}

